import firebase from "firebase";

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDSKoss8qklAlu35oW4nY1Dwv2RBpmE2qo",
  authDomain: "whatsapp-clone-955dd.firebaseapp.com",
  projectId: "whatsapp-clone-955dd",
  storageBucket: "whatsapp-clone-955dd.appspot.com",
  messagingSenderId: "1045857234713",
  appId: "1:1045857234713:web:c1da2f2a8a9ac3c4b7df88",
  measurementId: "G-RVWVZTLDS0",
};

const firebaseapp = firebase.initializeApp(firebaseConfig);
const db = firebaseapp.firestore();
const auth = firebase.auth();
const provider = new firebase.auth.GoogleAuthProvider();
export { auth, provider };
export default db;
